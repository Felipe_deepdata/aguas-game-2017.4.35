﻿using UnityEngine;
using System.Collections;

public class SelectLevelClick : MonoBehaviour {
	[Tooltip("ALWAYS name your levels by the following convention: Level + levelNumber. And keep them in the correct order: first level name -> Level1, second level name -> Level2, third level name -> Level3, etc.")]
	public string LevelName;

	[Tooltip("Maximum obtainable score in this level.")]
	public int levelArguments;
	public SpriteRenderer star1;
	public SpriteRenderer star2;
	public SpriteRenderer star3;
	public Sprite earnedStar;
	public Sprite notEarnedStar;
	public GameObject lockedIcon;
	public TextMesh levelNumberMesh;
	public int lvlNumber;


	//check / unckech which levels you want to be locked by default, than run the level select scene, uncommenting 
	//the "PlayerPrefs.DeleteAll();" code line in Start method to reset all the levels hierarchy
	[Tooltip("If the level is to be locked or not, by default")]
	public bool lockedByDefault;

	[Tooltip("Set this level as the last level of the game.")]
	public bool isLastLevel;

	public AudioClip ClickSound;

	void Start () {
	
		//reset the player prefs
		//PlayerPrefs.DeleteAll();

		if(isLastLevel)
		{
			//set the name of the last level in player pref
			PlayerPrefs.SetString("LastLevelName", LevelName);
		}
		if(LevelName != "")
		{
			PlayerPrefs.SetInt(LevelName + "_Arguments", levelArguments);
		}
		else
		{
			Debug.Log ("SelectLevelClick.cs script attached to " + gameObject.name + ", LevelName variable,  must not be null!!");
		}

		

		//if the key for THIS level is not setup in player pref, it means the player is running the game now for the first time,
		//so we need to setup the levels
		if(!PlayerPrefs.HasKey(LevelName))
		{
			Initial_Level_Setup();
		}

        //unlock all levels, with 0 stars each
        //Initial_Level_Setup_UNLOCKALL();
        //unlock all levels with random number of stars each
        //Initial_Level_Setup_UNLOCKALL(true);
		if (ApplicationModel.LivePlay){
			// Initial_Level_Setup ();
		}
		else{
			Initial_Level_Setup_UNLOCKALL (false);
		}


		//level 1 is always unlocked
		Debug.Log("DESBLOQUEO DE NIVEEEEL!!!!!!"+LevelName);
		Debug.Log("NIVEL letra "+LevelName +" -- "+ PlayerPrefs.GetString(LevelName, "unlocked"));
		// Debug.Log("NIVEL numero"+LevelName +" -- "+ PlayerPrefs.GetInt(LevelName, 0));
		// Debug.Log(PlayerPrefs.GetString(LevelName, "unlocked"));
		//if the current level is unlocked
		if(PlayerPrefs.GetString(LevelName, "unlocked") == "unlocked")
		{	
			// lockedByDefault = false;
			//display the level number
			levelNumberMesh.gameObject.SetActive(true);
			levelNumberMesh.text = lvlNumber.ToString();
			//hide the locked icon
			lockedIcon.SetActive(false);

			if(PlayerPrefs.GetInt(LevelName + "StarCount", 0) == 0)
			{
//				Debug.Log (gameObject.name + " - 0 Stars");
				//set the stars for the current level
			}

			if(PlayerPrefs.GetInt(LevelName + "StarCount", 0) == 1)
			{
				//Debug.Log (gameObject.name + " - 1 Star");
				star1.sprite = earnedStar;
				star2.sprite = notEarnedStar;
				star3.sprite = notEarnedStar;

				//set the stars for the current level
			}
			if(PlayerPrefs.GetInt(LevelName + "StarCount", 0) == 2)
			{
				//set the stars for the current level
				star1.sprite = earnedStar;
				star2.sprite = earnedStar;
				star3.sprite = notEarnedStar;
				//Debug.Log (gameObject.name + " - 2 Stars");
			}
			if(PlayerPrefs.GetInt(LevelName + "StarCount", 0) == 3)
			{
				//set the stars for the current level
				star1.sprite = earnedStar;
				star2.sprite = earnedStar;
				star3.sprite = earnedStar;
			
				//Debug.Log (gameObject.name + " - 3 Stars");
			}
		}
		//if the current level is locked
		else
		{
			//set the locked icon for the level
			lockedIcon.SetActive(true);
			levelNumberMesh.gameObject.SetActive(false);
			//Debug.Log (gameObject.name + " - Level Locked");
		}
	}

	/// <summary>	/// 
	/// Lock all levels except for the first one, and set 0 earned stars for all
	/// </summary>
	void Initial_Level_Setup()
	{
		if(LevelName != "")
		{

			if(lockedByDefault)
				PlayerPrefs.SetString(LevelName, "locked");
			else
				PlayerPrefs.SetString(LevelName, "unlocked");

			PlayerPrefs.SetInt(LevelName + "StarCount", 0);
		}
		else
		{
			Debug.Log ("SelectLevelClick.cs script attached to " + gameObject.name + ", LevelName variable,  must not be null!!");
		}
	}

	/// <summary>	 
	/// Unlock all levels 
	/// </summary>
	/// <param name="giveStars">If set to <c>true</c> give a random number of stars stars.</param>
	void Initial_Level_Setup_UNLOCKALL(bool giveStars)
	{
		if(LevelName != "")
		{
			PlayerPrefs.SetString(LevelName, "unlocked");
			if(giveStars)
				PlayerPrefs.SetInt(LevelName + "StarCount", Random.Range(0,4));
			else
				PlayerPrefs.SetInt(LevelName + "StarCount", 0);
		}
		else
		{
			Debug.Log ("SelectLevelClick.cs script attached to " + gameObject.name + ", LevelName variable,  must not be null!!");
		}
	}

	
	
	/// <summary>
	/// Raises the mouse up event.
	/// </summary>
	void OnMouseUp()
	{ 
		if(LevelName != "")
		{
			//if the selected level is unlocked, or it is the first level, which is always unlocked
			if((PlayerPrefs.GetString(LevelName, "unlocked") == "unlocked") || LevelName == "Level")   
			{
		
				//we need to know what level the player is currently playing
				HelperScript.currentLevelNumber = lvlNumber;

				//we need to know max obtainable score when we calculate how many stars the 
				//user gets (CharacterControler.cs - code line 270 +)

				Application.LoadLevel(LevelName);	
			}
		}
		else
		{
			Debug.Log ("SelectLevelClick.cs script attached to " + gameObject.name + ", LevelName variable,  must not be null!!");
		}
	}

	void OnMouseDown(){


		GetComponent<AudioSource>().PlayOneShot(ClickSound, 1);
	}
}
