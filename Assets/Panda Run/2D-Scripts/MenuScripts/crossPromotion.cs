﻿using UnityEngine;
using System.Collections;

public class crossPromotion : MonoBehaviour {



	public AudioClip btnClickedSound; //audioclip for buttons
	public Sprite btnActive; //sprite representing active state of button
	private SpriteRenderer spriteRenderer;
	private Sprite initialSprite; 
	
	
	// Use this for initialization
	void Start () {
		initialSprite = this.GetComponent<SpriteRenderer>().sprite; //save object's sprite to an initial variable
		spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	/// <summary>
	/// Raises the mouse down event.
	/// </summary>
	void OnMouseDown()
	{	
		spriteRenderer.sprite = btnActive;	
		
	}
	
	
	/// <summary>
	/// Raises the mouse up event.
	/// </summary>
	void OnMouseUp()
	{
		
		spriteRenderer.sprite = initialSprite; //reset the button sprite to its initial normal state
		Application.OpenURL("https://www.facebook.com/Ingenious-Studio-1753220254902831/");
		
		
	}


}
