﻿using UnityEngine;
using System.Collections;
using PandaRun;

public class CameraStabilizer : MonoBehaviour {

	public Transform Parent;   //the transform of the parent (the Character )

	private Vector3 pos;      //relative position to check if the character hit any of the screens edges
	private float lastXPos;


	private GameObject bottomObject;    //invisible collider used to track the position of the character relative to the level, and move the camera accordingly



	// Use this for initialization
	void Start () {

		//gameObject.GetComponent<Renderer>().sortingLayerID = 5;
		bottomObject = GameObject.FindGameObjectWithTag("bottomObject");
	}
	
	// Update is called once per frame
	void Update () {	 



		 	pos = GetComponent<Camera>().WorldToViewportPoint(Parent.transform.position);
			
		//Debug.Log(pos);

//		if(pos.x < 0.0) Debug.Log("I am left of the camera's view.");
//		if(1.0 < pos.x) Debug.Log("I am right of the camera's view.");
//		if(pos.y < 0.0) Debug.Log("I am below the camera's view.");
//		if(1.0 < pos.y) Debug.Log("I am above the camera's view.");
		}
				

	void LateUpdate()
	{
		if(!PandaRun.CharacterController.isDead)
		{


			if(!PandaRun.CharacterController.stopCameraMark)
			{
				lastXPos = transform.position.x;
		//if is at the top edge of the camera, follow the character when jumping
			if(pos.y < 0.923 && transform.position.y > 3.5f) 
			{

				transform.position = new Vector3(transform.position.x, 4f - Time.deltaTime, transform.position.z);
				if(transform.position.y < 4f)
				{

					transform.position = new Vector3(transform.position.x, 4f, transform.position.z);
				}
			}
			 
			//if he descanded and is under the collider, reset the camera coordinates and lock it on y axis
			if(Parent.transform.position.y < bottomObject.transform.position.y)
			{

				if(!(transform.position.y > 3.99f))
						transform.position = new Vector3(transform.position.x, transform.position.y + Time.deltaTime *15f, transform.position.z);
						
			}
			}
			//stop following on x axis
			else if(PandaRun.CharacterController.stopCameraMark)
			{
				//if is at the top edge of the camera, follow the character when jumping
				if(pos.y < 0.923 && transform.position.y > 3.5f) 
				{
					
					transform.position = new Vector3(lastXPos, 4f - Time.deltaTime, transform.position.z);
					if(transform.position.y < 4f)
					{
						
						transform.position = new Vector3(lastXPos, 4f, transform.position.z);
					}
				}
				
				//if he descanded and is under the collider, reset the camera coordinates and lock it on y axis
				if(Parent.transform.position.y < bottomObject.transform.position.y)
				{
					
					if(!(transform.position.y > 3.99f))
						transform.position = new Vector3(lastXPos, transform.position.y + Time.deltaTime *15f, transform.position.z);
					
				}
			}
		}

		else 
		{
			transform.position = new Vector3(transform.position.x, 3.692858f, transform.position.z);
		}




	}


}
