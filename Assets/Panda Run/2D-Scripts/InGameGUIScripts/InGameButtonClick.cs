﻿using UnityEngine;
using System.Collections;
using PandaRun;

public class InGameButtonClick : MonoBehaviour {

	public bool pauseButton;
	public bool quitToMainMenu;
	public bool resumeButton;
	public bool restartButton;
	public GameObject pauseMenuHUD;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (pauseButton);
	}


	void OnMouseDown()
	{	
		Debug.Log ("aaa");
		ButtonAction();		
	}


	void ButtonAction()
	{
		//if pauseButton is pressed, pause the game 
		if (pauseButton){ 
			PandaRun.CharacterController.canMove = false; 
			pauseMenuHUD.SetActive(true);		
		}
		if (resumeButton){ 
			PandaRun.CharacterController.canMove = true; 
			pauseMenuHUD.SetActive(false);	

		}
		if (restartButton){ 
			//PandaRun.CharacterController.canMove = true; 
			Application.LoadLevel(Application.loadedLevelName);		
		}
		if (quitToMainMenu){ 
			PandaRun.CharacterController.canMove = true; 
			Application.LoadLevel("MenuScene");		
		}

			

	}

}
