﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Facebook.Unity;

public class _listFriend
{
    private string _key;
    private string _id;
    private string _name;
    private Texture2D _avata;
    private bool _check;

    public void New()
    {

    }

    public void New(string key, string id, string name, Texture2D avata, bool check)
    {
        _key = key;
        _id = id;
        _name = name;
        _avata = avata;
        _check = check;
    }

    public string key
    {
        get
        {
            return _key;
        }
        set
        {
            _key = value;
        }
    }

    public string id
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }

    public string name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    public Texture2D avata
    {
        get
        {
            return _avata;
        }
        set
        {
            _avata = value;
        }
    }

    public bool check
    {
        get
        {
            return _check;
        }
        set
        {
            _check = value;
        }
    }
}

public class listFriend
{

    private List<_listFriend> _items;

    public listFriend()
    {
        if (this._items == null)
        {
            this._items = new List<_listFriend>();
        }
    }

    public void insertItem(string key, string id, string name, Texture2D avata, bool check)
    {
        _listFriend newItem = new _listFriend();
        newItem.key = key;
        newItem.id = id;
        newItem.name = name;
        newItem.avata = avata;
        newItem.check = check;
        _items.Add(newItem);
    }

    public void changeSelect(string key, bool select)
    {
        foreach (_listFriend item in _items)
        {
            if (item.key == key)
            {
                item.check = select;
                break;
            }
        }
    }

    public void selectAll(bool select)
    {
        foreach (_listFriend item in _items)
        {
            item.check = select;
        }
    }

    public bool checkAll()
    {
        bool status = true;
        foreach (_listFriend item in _items)
        {
            if (item.check == false)
            {
                status = false;
                break;
            }
        }
        return status;
    }

    public string[] getIdByCheck()
    {
        int count = 0;
        foreach (_listFriend item in _items)
        {
            if (item.check == true)
            {
                count++;
            }
        }
        int index = 0;
        string[] listID = new string[count];
        foreach (_listFriend item in _items)
        {
            if (item.check == true)
            {
                listID[index] = item.id;
                index++;
            }
        }

        return listID;
    }

    public int countItem()
    {
        int count = 0;
        foreach (_listFriend item in _items)
        {
            if (item != null)
            {
                count++;
            }
        }
        return count;
    }

    public _listFriend getItemByIndex(int index)
    {
        int count = 0;
        _listFriend itemResult = null;
        foreach (_listFriend item in _items)
        {
            if (count == index)
            {
                itemResult = item;
            }
            count++;
        }
        return itemResult;
    }

    public List<_listFriend> getAllItem()
    {
        return _items;
    }
}

public class csLoadImage
{
    private listFriend _listFriend;
    private string _key;
    private string _id;
    private string _name;
    private Texture2D _viewPhoto;
    private RawImage _viewPhoto2;

    public csLoadImage(listFriend listFriend, string key, string id, string name, RawImage viewPhoto)
    {
        _listFriend = listFriend;
        _key = key;
        _id = id;
        _name = name;
        try
        {
            _viewPhoto2 = viewPhoto;
        }
        catch { }
    }

    public void profilePhoto(IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
        {
            try
            {
                _viewPhoto = result.Texture;
                _viewPhoto2.texture = result.Texture;
            }
            catch
            {
                _viewPhoto = result.Texture;
            }
            _listFriend.insertItem(_key, _id, _name, _viewPhoto, false);
        }
    }

    public IEnumerator routinePhoto(string url)
    {
        WWW www = new WWW(url);
        yield return www;
        try
        {
            _viewPhoto = www.texture;
            _viewPhoto2.texture = www.texture;
        }
        catch
        {
            _viewPhoto = www.texture;
        }

        _listFriend.insertItem(_key, _id, _name, _viewPhoto, false);
    }
}

public class unicode
{
    public static string Remove(string text)
    {
        text = text.ToLower();
        string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
    "đ",
    "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
    "í","ì","ỉ","ĩ","ị",
    "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
    "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
    "ý","ỳ","ỷ","ỹ","ỵ",};
        string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
    "d",
    "e","e","e","e","e","e","e","e","e","e","e",
    "i","i","i","i","i",
    "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
    "u","u","u","u","u","u","u","u","u","u","u",
    "y","y","y","y","y",};
        for (int i = 0; i < arr1.Length; i++)
        {
            text = text.Replace(arr1[i], arr2[i]);
            text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
        }
        return text;
    }
}