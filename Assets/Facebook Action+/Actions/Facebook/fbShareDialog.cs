﻿using Facebook.Unity;
using System;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook Share Link")]
    public class fbShareLink : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string shareLinkDes = "Share articles, photos, videos and other content as a URL that points to the page where your content lives.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbShareLinkURL;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbShareLinkURL = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.ShareLink(new Uri(fbShareLinkURL.Value), callback: handleResult);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Share Custom")]
    public class fbShareCustom : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string shareLinkDes = "Share articles, photos, videos and other content as a URL that points to the page where your content lives with a picture.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbCustomShareLinkURL;
        [RequiredField]
        public FsmString fbCustomShareImageURL;

        public FsmString fbCustomShareTitle;
        public FsmString fbCustomShareDescription;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbCustomShareLinkURL = null;
            fbCustomShareImageURL = null;
            fbCustomShareTitle = null;
            fbCustomShareDescription = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.ShareLink(new Uri(fbCustomShareLinkURL.Value), fbCustomShareTitle.Value,
                    fbCustomShareDescription.Value, new Uri(fbCustomShareImageURL.Value), callback: handleResult);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Feed Share")]
    public class fbFeedShare : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string feedShareDes = "Prompts a user to post a feed story. It can be posted to either his/her own timeline (if toId is blank, or set to the user's own Facebook user ID), or to that of one friend (if toID is set to the user ID of that friend).";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbFeedShareLinkURL;
        [RequiredField]
        public FsmString fbFeedShareImageURL;

        public FsmString fbFeedShareTold;
        public FsmString fbFeedShareTitle;
        public FsmString fbFeedShareCaption;
        public FsmString fbFeedShareDescription;
        public FsmString fbFeedShareMediaURL;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbFeedShareLinkURL = null;
            fbFeedShareImageURL = null;
            fbFeedShareTold = null;
            fbFeedShareTitle = null;
            fbFeedShareCaption = null;
            fbFeedShareDescription = null;
            fbFeedShareMediaURL = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.FeedShare(fbFeedShareTold.Value, new Uri(fbFeedShareLinkURL.Value),
                    fbFeedShareTitle.Value, fbFeedShareCaption.Value, fbFeedShareDescription.Value,
                    new Uri(fbFeedShareImageURL.Value), fbFeedShareMediaURL.Value, callback: handleResult);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }
}
