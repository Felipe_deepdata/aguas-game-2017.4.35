﻿using UnityEngine;
using Facebook.Unity;
using System.Collections.Generic;
using System.Linq;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook App Request Filter None")]

    public class fbAppRequestFilterNone : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string FilterNoneDes = "Prompts the someone using your app to send game requests , short messages between users. Allow a player to recommend your game to friends, which he/she will choose from the Facebook-provided friend picker dialog.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbAppReqNoneMessage;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        [RequiredField]
        public FsmArray fbResultRequestTo;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultRequest;

        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        fbCustomFriend customFriend;
        bool loadFriend = true;
        float timeRate = 0.5f, nextTime = 0.0f;
        bool loadFinish = false, isSend = false;
        bool autoLogFace = true, run_func = true, isLogin = true;


        public override void Reset()
        {
            fbAppReqNoneMessage = null;
            fbResultRequestTo = null;
            fbResultRequest = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            loadFriend = true;
            nextTime = 0.0f;
            loadFinish = false;
            isSend = false;
            fbAutoLogin = null;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        //public override void OnEnter()
        //{
        //    if (!FB.IsInitialized || !FB.IsLoggedIn)
        //    {
        //        if (FsmEvent.IsNullOrEmpty(errorEvent))
        //            Fsm.Event(errorEvent);
        //    }
        //    else
        //    {
        //        FB.AppRequest(fbAppReqNoneMessage.Value, callback: handleResult);
        //    }
        //    Finish();
        //}

        private void _function()
        {
            if (loadFriend)
            {
                customFriend = Fsm.GameObject.AddComponent<fbCustomFriend>();
                customFriend.apiFriend = "me/taggable_friends";
                customFriend.start();
                loadFriend = false;
            }

            if (!loadFinish)
            {
                if (Time.time > nextTime)
                {
                    loadFinish = customFriend.loadFinish;
                    nextTime = Time.time + timeRate;
                }
            }
            else
            {
                if (!isSend)
                {
                    customFriend.objCustomDialog.SetActive(true);
                    isSend = true;
                }

                if (customFriend.selectFinish)
                {
                    appRequest();
                }
            }
        }

        void appRequest()
        {
            string[] listID = null;
            if(customFriend.listID.Length > 0)
            {
                listID = customFriend.listID;
            }
            FB.AppRequest(fbAppReqNoneMessage.Value, listID, callback: handleResult);

            Finish();
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                var jsonJesult = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as Dictionary<string, object>;

                if (jsonJesult.ContainsKey("to"))
                {
                    string stringToList = (jsonJesult["to"] as object).ToString();
                    strSplit(stringToList);
                }

                if (jsonJesult.ContainsKey("request"))
                    fbResultRequest.Value = (jsonJesult["request"] as object).ToString();

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }
        }

        void strSplit(string stringToSplit)
        {
            var trimCharsArray = ",".ToCharArray();
            fbResultRequestTo.stringValues = stringToSplit.Split(",".ToCharArray());
            for (var i = 0; i < fbResultRequestTo.stringValues.Length; i++)
            {
                var s = fbResultRequestTo.stringValues[i];
                fbResultRequestTo.Set(i, s.Trim(trimCharsArray));
            }
            fbResultRequestTo.SaveChanges();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook App Request Filter User")]
    public class fbAppRequestFilterUser : FsmStateAction
    {
        public class typeFilter
        {
            public enum Filter { FilterUserInApp, FilterUserOutApp }
        }

        [UIHint(UIHint.Description)]
        public string FilterUserDes = "Prompts the someone using your app to send game requests , short messages between users. Allow a player to recommend your game to friends who not play or also play your game using the Facebook-provided friend picker dialog.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        public typeFilter.Filter filterType = typeFilter.Filter.FilterUserInApp;

        [RequiredField]
        public FsmString fbAppReqUserTitle;
        [RequiredField]
        public FsmString fbAppReqUserMessage;

        public FsmString fbAppReqUserData;
        public FsmInt fbAppReqUserRecipient;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        [RequiredField]
        public FsmArray fbResultRequestTo;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultRequest;

        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        fbCustomFriend customFriend;
        bool loadFriend = true;
        float timeRate = 0.5f, nextTime = 0.0f;
        bool loadFinish = false, isSend = false;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbAppReqUserTitle = null;
            fbAppReqUserMessage = null;
            fbAppReqUserData = null;
            fbAppReqUserRecipient = null;
            fbResultRequestTo = null;
            fbResultRequest = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            loadFriend = true;
            nextTime = 0.0f;
            fbAutoLogin = null;
            loadFinish = false; isSend = false;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        //public override void OnEnter()
        //{
        //    if (!FB.IsInitialized || !FB.IsLoggedIn)
        //    {
        //        if (FsmEvent.IsNullOrEmpty(errorEvent))
        //            Fsm.Event(errorEvent);
        //    }
        //    else
        //    {
        //        List<object> filter;
        //        switch (filterType.ToString())
        //        {
        //            case "FilterUserOutApp":
        //                filter = new List<object>() { "app_non_users" };
        //                break;
        //            default:
        //                filter  = new List<object>() { "app_users" };
        //                break;
        //        }

        //        FB.AppRequest(fbAppReqUserMessage.Value, null, filter, null, fbAppReqUserRecipient.Value,
        //            fbAppReqUserData.Value, fbAppReqUserTitle.Value, handleResult);
        //    }

        //    Finish();
        //}

        private void _function()
        {
            if (loadFriend)
            {
                customFriend = Fsm.GameObject.AddComponent<fbCustomFriend>();
                switch (filterType.ToString())
                {
                    case "FilterUserOutApp":
                        customFriend.apiFriend = "me/invitable_friends";
                        break;
                    default:
                        customFriend.apiFriend = "me/friends";
                        break;
                }

                customFriend.start();
                loadFriend = false;
            }

            if (!loadFinish)
            {
                if (Time.time > nextTime)
                {
                    loadFinish = customFriend.loadFinish;
                    nextTime = Time.time + timeRate;
                }
            }
            else
            {
                if (!isSend)
                {
                    customFriend.objCustomDialog.SetActive(true);
                    isSend = true;
                }

                if (customFriend.selectFinish)
                {
                    appRequest();
                }
            }
        }

        void appRequest()
        {
            string[] listID = null;
            if(customFriend.listID.Length > 0)
            {
                listID = customFriend.listID;
            }

            FB.AppRequest(fbAppReqUserMessage.Value, listID, null, null, fbAppReqUserRecipient.Value,
                    fbAppReqUserData.Value, fbAppReqUserTitle.Value, handleResult);

            Finish();
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                var jsonJesult = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as Dictionary<string, object>;

                if (jsonJesult.ContainsKey("to"))
                {
                    string stringToSplit = (jsonJesult["to"] as object).ToString();
                    strSplit(stringToSplit);
                }

                if (jsonJesult.ContainsKey("request"))
                    fbResultRequest.Value = (jsonJesult["request"] as object).ToString();

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }
        }

        void strSplit(string stringToSplit)
        {
            var trimCharsArray = ",".ToCharArray();
            fbResultRequestTo.stringValues = stringToSplit.Split(",".ToCharArray());
            for (var i = 0; i < fbResultRequestTo.stringValues.Length; i++)
            {
                var s = fbResultRequestTo.stringValues[i];
                fbResultRequestTo.Set(i, s.Trim(trimCharsArray));
            }
            fbResultRequestTo.SaveChanges();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook App Request Filter Custom")]
    public class fbAppRequestFilterCustom : FsmStateAction
    {
        public class typeAction
        {
            public enum Action { None, ASKFOR, SEND, TURN }
        }

        [UIHint(UIHint.Description)]
        public string FilterUserDes = "Prompts the someone using your app to send game requests , short messages between users. Allow a player to send a game request to friends, which he/she will choose from the Facebook-provided friend picker dialog.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        public typeAction.Action actionType = typeAction.Action.None;

        [RequiredField]
        public FsmString fbAppReqCusTitle;
        [RequiredField]
        public FsmString fbAppReqCusMessage;

        public FsmString fbAppReqCusObjectID;
        //public FsmString fbAppReqCusRequestTo;
        public FsmString fbAppReqCusData;
        //public FsmString fbAppReqCusFilter;
        public FsmString fbAppReqCusExcludes;
        public FsmString fbAppReqCusRequestMax;
        public FsmInt fbAppReqCusRecipient;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        [RequiredField]
        public FsmArray fbResultRequestTo;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultRequest;

        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        fbCustomFriend customFriend;
        bool loadFriend = true;
        float timeRate = 0.5f, nextTime = 0.0f;
        bool loadFinish = false, isSend = false;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbAppReqCusTitle = null;
            fbAppReqCusMessage = null;
            fbAppReqCusObjectID = null;
            fbAppReqCusData = null;
            fbAppReqCusExcludes = null;
            fbAppReqCusRequestMax = null;
            fbAppReqCusRecipient = null;
            fbResultRequestTo = null;
            fbResultRequest = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            loadFriend = true;
            nextTime = 0.0f;
            loadFinish = false; isSend = false;
            fbAutoLogin = null;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        //public override void OnEnter()
        //{
        //    if (!FB.IsInitialized || !FB.IsLoggedIn)
        //    {
        //        if (FsmEvent.IsNullOrEmpty(errorEvent))
        //            Fsm.Event(errorEvent);
        //    }
        //    else
        //    {
        //        OGActionType? action = GetSelectedOGActionType();
        //        switch (actionType.ToString())
        //        {
        //            case "ASKFOR":
        //                action = OGActionType.ASKFOR;
        //                break;
        //            case "SEND":
        //                action = OGActionType.SEND;
        //                break;
        //            case "TURN":
        //                action = OGActionType.TURN;
        //                break;
        //            default:
        //                action = null;
        //                break;
        //        }

        //        if (action != null)
        //        {
        //            FB.AppRequest(
        //                fbAppReqCusMessage.Value,
        //                action.Value,
        //                fbAppReqCusObjectID.Value,
        //                null,//fbAppReqCusRequestTo != null ? fbAppReqCusRequestTo.Value.Split(',') : null,
        //                fbAppReqCusData.Value,
        //                fbAppReqCusTitle.Value,
        //                handleResult);

        //        }
        //        else
        //        {
        //            FB.AppRequest(
        //                fbAppReqCusMessage.Value,
        //                null,//string.IsNullOrEmpty(fbAppReqCusRequestTo.Value) ? null : fbAppReqCusRequestTo.Value.Split(','),
        //                null,//string.IsNullOrEmpty(fbAppReqCusFilter.Value) ? null : fbAppReqCusFilter.Value.Split(',').OfType<object>().ToList(),
        //                string.IsNullOrEmpty(fbAppReqCusExcludes.Value) ? null : fbAppReqCusExcludes.Value.Split(','),
        //                string.IsNullOrEmpty(fbAppReqCusRequestMax.Value) ? 0 : int.Parse(fbAppReqCusRequestMax.Value),
        //                fbAppReqCusData.Value,
        //                fbAppReqCusTitle.Value,
        //                handleResult);
        //        }
        //    }

        //    Finish();
        //}

        private void _function()
        {
            if (loadFriend)
            {
                customFriend = Fsm.GameObject.AddComponent<fbCustomFriend>();
                customFriend.apiFriend = "me/taggable_friends";
                customFriend.start();
                loadFriend = false;
            }

            if (!loadFinish)
            {
                if (Time.time > nextTime)
                {
                    loadFinish = customFriend.loadFinish;
                    nextTime = Time.time + timeRate;
                }
            }
            else
            {
                if (!isSend)
                {
                    customFriend.objCustomDialog.SetActive(true);
                    isSend = true;
                }

                if (customFriend.selectFinish)
                {
                    appRequest();
                }
            }
        }

        void appRequest()
        {
            string[] listID = null;
            if (customFriend.listID.Length > 0)
            {
                listID = customFriend.listID;
            }
                OGActionType? action = GetSelectedOGActionType();
                switch (actionType.ToString())
                {
                    case "ASKFOR":
                        action = OGActionType.ASKFOR;
                        break;
                    case "SEND":
                        action = OGActionType.SEND;
                        break;
                    case "TURN":
                        action = OGActionType.TURN;
                        break;
                    default:
                        action = null;
                        break;
                }

                if (action != null)
                {
                    FB.AppRequest(
                        fbAppReqCusMessage.Value,
                        action.Value,
                        fbAppReqCusObjectID.Value,
                        listID,//fbAppReqCusRequestTo != null ? fbAppReqCusRequestTo.Value.Split(',') : null,
                        fbAppReqCusData.Value,
                        fbAppReqCusTitle.Value,
                        handleResult);

                }
                else
                {
                    FB.AppRequest(
                        fbAppReqCusMessage.Value,
                        listID,//string.IsNullOrEmpty(fbAppReqCusRequestTo.Value) ? null : fbAppReqCusRequestTo.Value.Split(','),
                        null,//string.IsNullOrEmpty(fbAppReqCusFilter.Value) ? null : fbAppReqCusFilter.Value.Split(',').OfType<object>().ToList(),
                        string.IsNullOrEmpty(fbAppReqCusExcludes.Value) ? null : fbAppReqCusExcludes.Value.Split(','),
                        string.IsNullOrEmpty(fbAppReqCusRequestMax.Value) ? 0 : int.Parse(fbAppReqCusRequestMax.Value),
                        fbAppReqCusData.Value,
                        fbAppReqCusTitle.Value,
                        handleResult);
                }

                Finish();
        }

        OGActionType? GetSelectedOGActionType()
        {
            string[] actionTypeStrings ={
            "NONE",
            OGActionType.SEND.ToString(),
            OGActionType.ASKFOR.ToString(),
            OGActionType.TURN.ToString()
            };
            int selectedAction = 0;

            string actionString = actionTypeStrings[selectedAction];
            if (actionString == OGActionType.SEND.ToString())
            {
                return OGActionType.SEND;
            }
            else if (actionString == OGActionType.ASKFOR.ToString())
            {
                return OGActionType.ASKFOR;
            }
            else if (actionString == OGActionType.TURN.ToString())
            {
                return OGActionType.TURN;
            }
            else
            {
                return null;
            }
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                var jsonJesult = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as Dictionary<string, object>;

                if (jsonJesult.ContainsKey("to"))
                {
                    string stringToSplit = (jsonJesult["to"] as object).ToString();
                    strSplit(stringToSplit);
                }

                if (jsonJesult.ContainsKey("request"))
                    fbResultRequest.Value = (jsonJesult["request"] as object).ToString();

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }
        }

        void strSplit(string stringToSplit)
        {
            var trimCharsArray = ",".ToCharArray();
            fbResultRequestTo.stringValues = stringToSplit.Split(",".ToCharArray());
            for (var i = 0; i < fbResultRequestTo.stringValues.Length; i++)
            {
                var s = fbResultRequestTo.stringValues[i];
                fbResultRequestTo.Set(i, s.Trim(trimCharsArray));
            }
            fbResultRequestTo.SaveChanges();
        }
    }
}
