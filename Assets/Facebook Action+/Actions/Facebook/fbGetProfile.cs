﻿using Facebook.Unity;
using UnityEngine;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook Get Infomation Profile")]
    public class fbGetProfile : FsmStateAction
    {
        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        public FsmString fbResultUserID;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultFirstName;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultLastName;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultGender;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultLocale;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultEmail;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultBirthday;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultTimezone;
        [UIHint(UIHint.Variable)]
        public FsmString fbResultWebsite;
        public FsmTexture fbResultProfilePhoto;

        [UIHint(UIHint.Description)]
		public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbAutoLogin = null;
            fbResultUserID = null;
            fbResultFirstName = null;
            fbResultLastName = null;
            fbResultGender = null;
            fbResultLocale = null;
            fbResultEmail = null;
            fbResultBirthday = null;
            fbResultTimezone = null;
            fbResultWebsite = null;
            fbResultProfilePhoto = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnGUI()
        {
            if (fbResultProfilePhoto.Value == null)
            {
                return;
            }

            Rect rect = new Rect(new Vector2(Screen.width, Screen.height), new Vector2(Screen.width, Screen.height));
            ScaleMode scaleMode = ScaleMode.StretchToFill;

            GUI.DrawTexture(rect, fbResultProfilePhoto.Value, scaleMode, true, 0);

        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.API("/me/picture", HttpMethod.GET, callback: ProfilePhotoCallback);
            FB.API("me?fields=id,name,last_name,first_name,gender,locale,email,birthday,timezone,website", HttpMethod.GET, handleResult);
        }

        void ProfilePhotoCallback(IGraphResult result)
        {
            if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
            {
                fbResultProfilePhoto.Value = result.Texture;

            }
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
				fbRawResult = result.RawResult;
                var jsonJesult = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as Dictionary<string, object>;

                if (jsonJesult.ContainsKey("id"))
                    fbResultUserID.Value = (jsonJesult["id"] as object).ToString();

                if (jsonJesult.ContainsKey("first_name"))
                    fbResultFirstName.Value = (jsonJesult["first_name"] as object).ToString();

                if (jsonJesult.ContainsKey("last_name"))
                    fbResultLastName.Value = (jsonJesult["last_name"] as object).ToString();

                if (jsonJesult.ContainsKey("gender"))
                    fbResultGender.Value = (jsonJesult["gender"] as object).ToString();

                if (jsonJesult.ContainsKey("locale"))
                    fbResultLocale.Value = (jsonJesult["locale"] as object).ToString();

                if (jsonJesult.ContainsKey("email"))
                    fbResultEmail.Value = (jsonJesult["email"] as object).ToString();

                if (jsonJesult.ContainsKey("birthday"))
                    fbResultBirthday.Value = (jsonJesult["birthday"] as object).ToString();

                if (jsonJesult.ContainsKey("timezone"))
                    fbResultTimezone.Value = (jsonJesult["timezone"] as object).ToString();

                if (jsonJesult.ContainsKey("website"))
                    fbResultWebsite.Value = (jsonJesult["website"] as object).ToString();

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                //"Empty Response";
            }

            Finish();
        }
    }
}
