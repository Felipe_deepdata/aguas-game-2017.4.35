﻿using UnityEngine;
using Facebook.Unity;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook AccessToken")]
    public class fbAccessToken : FsmStateAction
    {
        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        public FsmString fbResultAccessToken;
        
        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;

        float timeRate = 1.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbResultAccessToken = null;
            successEvent = null;
            errorEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            if (!FB.IsLoggedIn)
            {
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else
            {
                fbResultAccessToken.Value = AccessToken.CurrentAccessToken.TokenString;

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }

            Finish();
        }
    }
}
