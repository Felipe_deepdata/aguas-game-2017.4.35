﻿using UnityEngine;
using System.Collections;

public class Garrafon : MonoBehaviour {

    public int GarrafonType = 0;
    float RotationalSpeed = 5f;
    float Speed = 5;
    private bool visible = false;
    private bool activated = false;
    Camera mainCamera;
    public GameObject FallingGarrafon;
    public float DelayToActivate;
    public float DistanceToActivate;
    float DistanceToActivateSquared;
    Rigidbody2D FallingGarrafonRB;

	// Use this for initialization
	void Start () {
        mainCamera = Camera.main;

        StartCoroutine("CheckDistance");

        if (FallingGarrafon != null)
        {
            FallingGarrafonRB = FallingGarrafon.GetComponent<Rigidbody2D>();
            FallingGarrafonRB.isKinematic = true;
        }
        DistanceToActivateSquared = Mathf.Pow(DistanceToActivate, 2);
    }

	void OnBecameVisible() {
        visible = true;
	}

    IEnumerator CheckDistance()
    {
        for (;;)
        {
            if (Vector3.SqrMagnitude(transform.position - mainCamera.transform.position) < DistanceToActivateSquared)
            {
                if(!activated)
                    switch (GarrafonType)
                    {
                        case 0:
                            Activate();
                            break;
                        case 1:
                            Activate();
                            break;
                        case 2:
                            Activate();
                            break;
                        case 3:
                            Activate();
                            break;
                        case 4:
                            Invoke("Activate", DelayToActivate);
                            break;
                        default:
                            Activate();
                            break;
                    }
            }
            yield return new WaitForSeconds(0.1f);
        }
    
    }

    void Activate() {
        activated = true;
    }
	
	// Update is called once per frame
	void Update () {
	
        

		switch (GarrafonType)
		{
		//Static garrafon
		case 0:
			break;
		//Spinning garrafon
		case 1:
			if (visible)
				transform.RotateAround (Vector3.forward, Time.deltaTime * RotationalSpeed);
			
            if (activated)
                transform.position += Vector3.left * Time.deltaTime * Speed;

            break;
		//Water Jet garrafon
		case 2:
			break;
		//Throwing garrafon
		case 3:
			break;
        //Falling garrafon
        case 4:
                if(activated)
                {
                    if (FallingGarrafon != null)
                    {
                        if (FallingGarrafonRB.isKinematic == true)
                        {
                            transform.RotateAround(Vector3.forward, Time.deltaTime * -4);
                            if (transform.localRotation.eulerAngles.z > 160f)
                            {

                                Debug.Log("addforce");

                                FallingGarrafonRB.isKinematic = false;
                                FallingGarrafonRB.AddForce(new Vector2(200, 0));


                            }
                        }
                    }
                    
                }
            break;
            default:
			
			break;
		}

	}
}
