﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicTrack : MonoBehaviour {

	public bool InMainMenu;
	// Use this for initialization

	void Start () {
		if (PlayerPrefs.GetInt ("MusicEnabled") == 0)
			GetComponent<AudioSource> ().Play ();
	}

	public void Play(){
		GetComponent<AudioSource> ().Play ();
	}


	public void Stop(){
		GetComponent<AudioSource> ().Stop ();
	}
	

}
