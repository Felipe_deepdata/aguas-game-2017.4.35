﻿using UnityEngine;
using System.Collections;

public class TruckExhaust : MonoBehaviour {

	private Vector3 Movement = new Vector3(-0.025f, 0.005f, 0f);
	// Use this for initialization
	void Start () {
		Invoke ("Autokill", 2f);
	}

	void Autokill(){
		Destroy (gameObject);

	}

	// Update is called once per frame
	void Update () {
		transform.position += Movement;
	}
}
